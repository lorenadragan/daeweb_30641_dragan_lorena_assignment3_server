﻿using daw3_server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace daw3_server.Repository
{
    public interface IRead<TModel>
        where TModel : class, IPersistableModel
    {
        TModel GetById(object Id);
        IQueryable<TModel> GetAll();
    }
}
