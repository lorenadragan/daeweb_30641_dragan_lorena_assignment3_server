﻿using daw3_server.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace daw3_server.Repository
{
    public class Repository<TModel> : ISave<TModel>, IRead<TModel>, ICommand<TModel>
        //in orice repo => decorator 
        where TModel : class, IPersistableModel
    {
        private dentalWEB_entities _context { get; set; }
        internal DbSet<TModel> _dbSet { get; set; }

        public Repository(dentalWEB_entities context)
        {
            this._context = context;
            this._dbSet = _context.Set<TModel>();
        }

        public void OnSaveValidateErrors(DbEntityValidationException dbEx)
        {
            foreach (var validationErrors in dbEx.EntityValidationErrors)
            {
                foreach (var validationError in validationErrors.ValidationErrors)
                {
                    Debug.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                }
            }

        }

        public void SaveChangesMethod()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                OnSaveValidateErrors(dbEx);
            }
        }

        public IQueryable<TModel> GetAll()
        {
            return _dbSet;
        }

        public TModel GetById(object Id)
        {
            return _dbSet.Find(Id);
        }

        public void Delete(object id)
        {
            TModel entityToDelete = _dbSet.Find(id);
            Delete(entityToDelete);
        }

        public void Delete(TModel entityToDelete)
        {
            _dbSet.Remove(entityToDelete);
        }

        public TModel Insert(TModel newObject)
        {
            _dbSet.Add(newObject); //adds a new instance of TModel to a context with Added EntityState => on SAveChanges() will be added in the db 

            return newObject;
        }

        public TModel Update(TModel obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            return obj;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_context != null)
                {
                    _context.Dispose();
                    _context = null;
                }
            }
        }
    }
}