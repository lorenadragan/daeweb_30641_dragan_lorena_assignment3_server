﻿using daw3_server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace daw3_server.Repository
{
    public interface ICommand<TModel>
        where TModel : class, IPersistableModel
    {
        TModel Insert(TModel newObject);

        TModel Update(TModel obj);

        void Delete(object id);

        void Delete(TModel entityToDelete);

        void Dispose(bool disposing);

    }
}
