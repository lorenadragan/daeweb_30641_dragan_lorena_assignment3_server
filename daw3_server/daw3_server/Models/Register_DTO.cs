﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace daw3_server.Models
{
    public class Register_DTO: IPersistableModel
    {
        public string email;
        public string password;
        public string name;

        public Register_DTO(string email, string password, string name)
        {
            this.email = email;
            this.password = password;
            this.name = name;
        }

        public Register_DTO() { }

    }
}