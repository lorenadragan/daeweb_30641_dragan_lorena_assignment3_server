﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace daw3_server.Models
{
    public class User_DTO
    {
        public int client_id;
        public string email;
        public string password;
        public int user_id;
        public string name;
        public List<service> services = new List<service>();

        public User_DTO(int client_id, string email, string password, int user_id, string name, List<service> services)
        {
            this.client_id = client_id;
            this.email = email;
            this.password = password;
            this.user_id = user_id;
            this.name = name;
            this.services = services;
        }

        public User_DTO() { }
    }
}