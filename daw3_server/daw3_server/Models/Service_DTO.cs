﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace daw3_server.Models
{
    public class Service_DTO
    {
        public int id;
        public int client_id;
        public int service_id;
        public string name;

        public Service_DTO(int id, int client_id, int service_id, string name)
        {
            this.id = id;
            this.client_id = id;
            this.service_id = service_id;
            this.name = name;
        }

        public Service_DTO() { }
    }
}