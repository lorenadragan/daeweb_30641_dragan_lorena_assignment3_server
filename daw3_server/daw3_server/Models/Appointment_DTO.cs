﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace daw3_server.Models
{
    public class Appointment_DTO
    {
        public int id;
        public int client_id;
        public System.DateTime date;
        public string doctor_name;

        public Appointment_DTO(int id, int client_id, DateTime date, string doctor_name)
        {
            this.id = id;
            this.client_id = client_id;
            this.date = date;
            this.doctor_name = doctor_name;
        }

        public Appointment_DTO() { }
    }
}