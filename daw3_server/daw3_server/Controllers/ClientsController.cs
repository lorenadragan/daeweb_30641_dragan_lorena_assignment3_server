﻿using daw3_server.Models;
using daw3_server.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace daw3_server.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ClientsController : ApiController
    {
        private Repository<user> _users;
        private Repository<client> _clients;
        dentalWEB_entities _context;

        public ClientsController()
        {
            this._context = new dentalWEB_entities();
            this._users = new Repository<user>(this._context);
            this._clients = new Repository<client>(this._context);
        }
        // GET: api/Clients
        [HttpGet]
        [Route("api/clients")]
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, this._clients.GetAll());
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // GET: api/Clients/5
        [HttpGet]
        [Route("api/client/{id}")]
        public HttpResponseMessage Get(int id)
        {
            var result = _clients.GetById(id);
            if (result != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "The client with the specified ID = " + id.ToString() + " not found");
            }
        }

        // POST: api/Clients
        //[HttpPost]
        //[Route("api/client/{id}")]
        //public void Post([FromBody]client client)
        //{
            
        //}

        // PUT: api/Clients/5
        [HttpPut]
        [Route("api/clients/{id}")]
        public HttpResponseMessage Put(int id, [FromBody]client updatedClient)
        {
            try
            {
                var toUpdate = _clients.GetById(id);
                if(toUpdate == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "The client with the specified ID = " + id + " Not Found");
                }
                else
                {
                    var clientName = updatedClient.name;
                    toUpdate.name = updatedClient.name;
                    _clients.Update(toUpdate);
                    _clients.SaveChangesMethod();

                    return Request.CreateResponse("The client with the specified id = " + updatedClient.id + " was successfully updated");

                }

            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // DELETE: api/Clients/5
        [HttpDelete]
        [Route("api/clients/{id}")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var toDeleteClient = _clients.GetById(id);
                if(toDeleteClient == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "The client with this ID does not exist");
                }
                _clients.Delete(id);
                _clients.SaveChangesMethod();

                return Request.CreateResponse(HttpStatusCode.OK, "The client with the specified ID = " + id + "was deleted with success");
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
