﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using daw3_server.Models;
using daw3_server.Repository;

namespace daw3_server.Controllers
{
    public class DoctorsController : ApiController
    {
        private Repository<doctor> _doctors;
        dentalWEB_entities _context;

        public DoctorsController()
        {
            this._context = new dentalWEB_entities();
            this._doctors = new Repository<doctor>(this._context);
        }

        // GET: api/Doctors
        [HttpGet]
        [Route("api/doctors")]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _doctors.GetAll().ToList());
        }

        // GET: api/Doctors/5
        [HttpGet]
        [Route("api/doctors/{id}")]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var doctor = _doctors.GetById(id);
                if (doctor != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, doctor);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Not found");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("api/doctors/getByName={name}")]
        public HttpResponseMessage GetByName(string name)
        {
            try
            {
                var doctor = _doctors.GetAll().ToList()
                    .Where(s => string.Equals(s.name, name, StringComparison.InvariantCultureIgnoreCase));
                if(doctor != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, doctor);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Not found");
                }

            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // POST: api/Doctors

        [HttpPost]
        [Route("api/doctors")]
        public HttpResponseMessage Post([FromBody]doctor value)
        {
            try
            {
                _doctors.Insert(value);
                _doctors.SaveChangesMethod();

                var message = Request.CreateResponse(HttpStatusCode.Created, value);
                message.Headers.Location = new Uri(Request.RequestUri + "Inserted");
                return message;
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // PUT: api/Doctors/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Doctors/5
        public void Delete(int id)
        {
        }
    }
}
