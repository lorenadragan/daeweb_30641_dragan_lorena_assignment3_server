﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using daw3_server.Models;
using daw3_server.Repository;

namespace daw3_server.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ServicesController : ApiController
    {
        private Repository<service> _services;
        dentalWEB_entities _context;

        public ServicesController()
        {
            this._context = new dentalWEB_entities();
            this._services = new Repository<service>(this._context);
        }


        // GET: api/Services
        [HttpGet]
        [Route("api/services")]
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _services.GetAll());
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // GET: api/Services/5
        [HttpGet]
        [Route("api/services/{id}")]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var service = _services.GetById(id);
                if(service == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "The service with the specified id = " + id + " not found");
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "The service with the specified ID = " + id.ToString() + " not found");
                }

            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("api/services/getName/{name}")]
        public HttpResponseMessage GetName(string name)
        {
            try
            {
                var serviceByName = _services.GetAll().ToList()
                    .Where(s => string.Equals(name, s.name)).First();
                if(serviceByName == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "The service with this name = " + name + "is not in the system");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, serviceByName);
                }
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }


        // POST: api/Services
        [HttpPost]
        [Route("api/services")]
        public HttpResponseMessage Post([FromBody]service value)
        {
            try
            {
                _services.Insert(value);
                _services.SaveChangesMethod();

                var message = Request.CreateResponse(HttpStatusCode.Created, value);
                message.Headers.Location = new Uri(Request.RequestUri + "Inserted");
                return message;
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // PUT: api/Services/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Services/5
        public void Delete(int id)
        {
        }
    }
}
