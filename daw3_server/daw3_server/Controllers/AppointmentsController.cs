﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using daw3_server.Models;
using daw3_server.Repository;

namespace daw3_server.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AppointmentsController : ApiController
    {
        private Repository<appointment> _appointments;
        private Repository<doctor> _doctors;
        dentalWEB_entities _context;

        public AppointmentsController()
        {
            this._context = new dentalWEB_entities();
            this._appointments = new Repository<appointment>(this._context);
            this._doctors = new Repository<doctor>(this._context);
        }


        // GET: api/Appointments
        [HttpGet]
        [Route("api/appointments")]
        public HttpResponseMessage Get()
        {
            
            return Request.CreateResponse(HttpStatusCode.OK, _appointments.GetAll().ToList());
        }

        [HttpGet]
        [Route("api/appointments/clientId={id}")]
        public HttpResponseMessage GetByClientId(int id)
        {
            try
            {
                var appointments_DTO = new List<Appointment_DTO>();
                var appointments_db = _appointments.GetAll().ToList()
                   .Where(s => s.clientId == id).ToList();
                foreach(var app in appointments_db)
                {
                    //gaseste nume medic, restul campurilor raman la fel
                    var doctorName = _doctors.GetById(app.medicId).name;
                    var newApp = new Appointment_DTO(app.id, app.clientId, app.date, doctorName);
                    appointments_DTO.Add(newApp);
                }

                //var appointmetsForGivenClient = _appointments.GetAll().ToList()
                //   .Where(s => s.clientId == id);

                return Request.CreateResponse(HttpStatusCode.OK, appointments_DTO);
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // GET: api/Appointments/5
        [HttpGet]
        [Route("api/appointments/{id}")]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var app = _appointments.GetById(id);
                if(app != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, app);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Not found");
                }
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // POST: api/Appointments
        [HttpPost]
        [Route("api/appointments")]
        public HttpResponseMessage Post([FromBody]appointment value)
        {
            try
            {
                _appointments.Insert(value);
                _appointments.SaveChangesMethod();


                var message = Request.CreateResponse(HttpStatusCode.Created, value);
                message.Headers.Location = new Uri(Request.RequestUri + value.id.ToString());
                return message;
            }
            catch (DbUpdateException)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid values ");
            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid operation");
            }
        }

        // PUT: api/Appointments/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Appointments/5
        [HttpDelete]
        [Route("api/appointments/{id}")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                _appointments.Delete(id);
                _appointments.SaveChangesMethod();

                return Request.CreateResponse(HttpStatusCode.OK, "deleted");
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
