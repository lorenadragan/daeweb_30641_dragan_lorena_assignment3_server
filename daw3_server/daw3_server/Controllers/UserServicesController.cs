﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using daw3_server.Models;
using daw3_server.Repository;

namespace daw3_server.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserServicesController : ApiController
    {
        private Repository<service> _services;
        private Repository<client> _clients;
        private Repository<user_service> _usersServices;
        dentalWEB_entities _context;

        public UserServicesController()
        {
            this._context = new dentalWEB_entities();
            this._services = new Repository<service>(this._context);
            this._clients = new Repository<client>(this._context);
            this._usersServices = new Repository<user_service>(this._context);
        }

        // GET: api/UserServices
        [HttpGet]
        [Route("api/usersServices")]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _usersServices.GetAll());
        }

        // GET: api/UserServices/5
        [HttpGet]
        [Route("api/usersServices/{id}")]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var service = _usersServices.GetById(id);
                if(service != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, service);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "");
                }
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("api/usersServices/clientId/{id}")]
        public HttpResponseMessage GetByClientId(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _usersServices.GetAll().ToList()
                    .Where(s => s.client_id == id).ToList());
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        // POST: api/UserServices
        [HttpPost]
        [Route("api/usersServices/clientId={clientId}/serviceId={serviceId}")]
        public HttpResponseMessage Post(int clientId, int serviceId)
        {
            try
            {
                var newUserServiceEntry = new user_service(clientId, serviceId);
                _usersServices.Insert(newUserServiceEntry);
                _usersServices.SaveChangesMethod();

                return Request.CreateResponse(HttpStatusCode.OK, "Inserted");
            }
            catch (DbUpdateException)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid values ");
            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid operation");
            }
        }

        // PUT: api/UserServices/5
        public void Put(int id, [FromBody]string value)
        {

        }

        // DELETE: api/UserServices/5
        [HttpDelete]
        [Route("api/usersServices/delete/clientId={clientId}/serviceId={serviceId}")]
        public HttpResponseMessage Delete(int clientId, int serviceId)
        {
            try
            {
                var toDelete = _usersServices.GetAll().ToList()
                    .Where(s => s.client_id == clientId)
                    .Where(s => s.service_id == serviceId).First();
                if(toDelete != null)
                {
                    _usersServices.Delete(toDelete.id);
                    _usersServices.SaveChangesMethod();

                    return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Not found");
                }
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
