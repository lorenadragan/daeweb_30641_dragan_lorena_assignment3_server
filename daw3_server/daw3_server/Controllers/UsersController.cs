﻿using daw3_server.Models;
using daw3_server.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace daw3_server.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersController : ApiController
    {
        private Repository<user> _users;
        private Repository<client> _clients;
        private Repository<service> _services;
        private Repository<user_service> _usersServices;
        dentalWEB_entities _context;

        public UsersController()
        {
            this._context = new dentalWEB_entities();
            this._users = new Repository<user>(this._context);
            this._services = new Repository<service>(this._context);
            this._usersServices = new Repository<user_service>(this._context);
            this._clients = new Repository<client>(this._context);

        }
        // GET: api/Users
        [HttpGet]
        [Route("api/users")]
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, this._users.GetAll());
            } catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpGet]
        [Route("api/users/email={email}/password={password}")]
        public HttpResponseMessage GetUserByCredentials(string email, string password)
        {
            try
            {
                foreach (var user in _users.GetAll().ToList())
                {
                    if (string.Equals(email, user.email, StringComparison.InvariantCultureIgnoreCase) &&
                        string.Equals(password, user.password, StringComparison.InvariantCultureIgnoreCase))
                    {
                        //iau contul de client aferent userului
                        var userId = user.id;
                        var loggedInUser = new User_DTO();
                        var client = this._clients.GetAll().ToList().Where(s => s.user_id == userId).First();
                        loggedInUser.client_id = client.id;
                        loggedInUser.email = email;
                        loggedInUser.password = password;
                        loggedInUser.user_id = userId;
                        loggedInUser.name = client.name;

                        //acum iau lista de servicii
                        var serv_clienti = _usersServices.GetAll().ToList()
                            .Where(s => s.client_id == loggedInUser.client_id).ToList();
                        var loggedInUserServices = new List<service>();
                        if (serv_clienti.Any())
                        {
                            foreach (var serviciu in serv_clienti)
                            {
                                var serviciuDefault = _services.GetById(serviciu.service_id);
                                loggedInUserServices.Add(serviciuDefault);
                            }
                        }
                        loggedInUser.services = loggedInUserServices;
                        return Request.CreateResponse(HttpStatusCode.OK, loggedInUser);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, "The user with this credentials is not in the system");
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // GET: api/Users/5
        [HttpGet]
        [Route("api/users/{id}")]
        public HttpResponseMessage Get(int id)
        {
            var result = _users.GetById(id);
            if(result != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "The user with the specified ID = " + id.ToString() + " not found");
            }
        }

        // POST: api/Users
        [HttpPost]
        [Route("api/users")]
        public HttpResponseMessage Post([FromBody]Register_DTO user)
        {
            try
            {
                //var userEmail = user.email;
                //var users = _users.GetAll().ToList();
                //foreach(var u in users)
                //{
                //    if(string.Equals(u.email, user.email))
                //    {
                //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "This email is already in the system");
                //    }
                //}
                //_users.Insert(user);
                //_users.SaveChangesMethod();
                //var message = Request.CreateResponse(HttpStatusCode.Created, user);
                //message.Headers.Location = new Uri(Request.RequestUri + user.id.ToString());
                //return message;
                var email = user.email;
                var password = user.password;
                var name = user.name;
                var users = _users.GetAll().ToList();
                foreach(var u in users)
                {
                    if(string.Equals(u.email, email, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "This email is already in the system");
                    }
                }

                var newUser = new user(email, password);
                _users.Insert(newUser);
                _users.SaveChangesMethod();

                var newClient = new client(name);
                //setez si campul de userId corespunzator in client
                var beforeInsertedUser = _users.GetAll().ToList().Where(s => string.Equals(email, s.email))
                    .Where(s => string.Equals(s.password, password)).First();
                newClient.user_id = beforeInsertedUser.id;
                _clients.Insert(newClient);
                _clients.SaveChangesMethod();
                var message = Request.CreateResponse(HttpStatusCode.Created, user);
                message.Headers.Location = new Uri(Request.RequestUri + newUser.id.ToString());
                return message;
            }
            catch (DbUpdateException)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid values");
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Operation");
            }
        }

        // PUT: api/Users/5
        [HttpPut]
        [Route("api/users/{id}")]
        public HttpResponseMessage Put(int id, [FromBody]user newUser)
        {
            try
            {
                var toUpdate = _users.GetById(id);
                if(toUpdate == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "The user with the specified ID = " + id + " Not Found");
                }
                else
                {
                    //var userEmail = newUser.email;
                    //var users = _users.GetAll().ToList();
                    //foreach (var u in users)
                    //{
                    //    if (string.Equals(u.email, newUser.email))
                    //    {
                    //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "This email is already in the system");
                    //    }
                    //}

                    toUpdate.email = newUser.email;
                    toUpdate.password = newUser.password;
                    _users.Update(toUpdate);
                    _users.SaveChangesMethod();

                    return Request.CreateResponse(HttpStatusCode.OK, "The user with the specified ID = " + id + " was successfully updated");
                }
            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid Request");
            }
        }

        // DELETE: api/Users/5
        [HttpDelete]
        [Route("api/users/{id}")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                _users.Delete(id);
                _users.SaveChangesMethod();

                return Request.CreateResponse(HttpStatusCode.OK, "The user with the specified ID = " + id.ToString() + " was deleted with success");
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
